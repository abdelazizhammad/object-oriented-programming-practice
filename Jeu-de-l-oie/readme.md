# TP Jeu de l'oie

**Khaled Altayeb Alrafei & Abdelziz HAMMAD AHMED**


### Le TP

L'objectif de ce TP est d'implémenter le jeu de l'oie avec plusieurs types de cellule et different règle selon le joueur

Pour cela on utilise des classes abstraites ,des classes héritages.

**Attention** : Avant de commencer,assurez-vous de l'existence du répertoire *classes/* et *docs/* à la racine du projet. On peut utiliser la commande :

	`$make folders`

### Documentation

Pour générer la documentation et la retrouver dans le dossier *docs* on réalise la commande :

	`$make doc`

*Note:* Les différentes commandes make se réalise à la racine du projet.

### Compilation

Pour compiler les différentes classes on réalise la commande :

	`$make compile`

On retrouve les fichiers compilés dans le répertoire *classes/*


### Les Tests

Pour compiler les classes de tests et exécuter les tests JUnit4 avec une fenêtre graphique, on utilise la commande :

	`$make test`

Pour executer :  exemple : $make CellTest


### Nettoyer le répertoire de travail

Pour nettoyer le répertoire de travail fichiers_agence on utilise la commande :

	`$make clean`

Elle a pour effet de supprimer les fichiers temporaires, les fichiers compilés .class, l(es) archive(s) jar (de ce projet, on conserve test-1.7.jar pour les tests) et la documentation qui peuvent être générés comme vu précedemment.

### Executable

Creer un executable :

		$make jar

Pour executer le jeu de l'oie:taper ce qui suit et ajouter après = le nombre de joueurs

		$make run ARGS=

exemple: `$make run ARGS=4`


### Réalisation

Quelques tests produisent des erreurs
