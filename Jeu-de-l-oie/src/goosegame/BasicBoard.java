package goosegame;
import goosegame.*;
/**
 * A traditional board of the goosegame
 *
 * @author Khaled Altayeb Alrafei & Abdelziz HAMMAD AHMED
 * @version 1.0
 */
public class BasicBoard extends Board{

  public BasicBoard(){
    super(63);
    super.theCells=new Cell[super.NbOfCell+1];
  }

/** Initialize the board with traditional rules
*/
  public void initBoard(){
    this.theCells[0] = new ZeroCell();
    for(int i = 1; i < this.NbOfCell+1; i++){
		    switch (i) {
	            case 9:case 18:case 27:case 36:case 45:case 54: // Goose Cells
	            	this.theCells[i] = new GooseCell(i);break;
	            case 31:case 52: //  Trap Cells
	            	this.theCells[i] = new TrapCell(i);break;
	            case 19: // Waiting Cell
	            	this.theCells[i] = new WaitingCell(i,2);break;
	            case 6:  // Teleport Cell
	            	this.theCells[i] = new TeleportCell(i, 12);break;
	            case 42:    // Teleport Cell
	            	this.theCells[i] = new TeleportCell(i, 30);break;
	            case 58:  // Teleport Cells
	            	this.theCells[i] = new TeleportCell(i, 1);break;
	            default : // Normal Cells
	            	this.theCells[i] = new Cell(i);break;
	        }
		}
	}
  }
