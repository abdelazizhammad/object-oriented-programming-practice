package goosegame;

import goosegame.*;

abstract public class Board{
protected final int  NbOfCell;
protected Cell[] theCells;


public Board(int NbOfCell){
  this.NbOfCell=NbOfCell;
	this.theCells=new Cell[NbOfCell+1];
	this.theCells[0] = new ZeroCell();
	}

  /**
	* initializes the board
	*/
protected abstract void initBoard();

/**
	* Returns the cell at <code>index</code>
	* @param index the index of the cell
	* @return the cell at <code>index</code>
	*/
public Cell getCell(int index){
  return this.theCells[index];
}

/**
	* Returns the number of cells in the board (not counting the start cell)
	*  @return the number of cells in the board
	*/
public int getNbOfCells(){
  return this.NbOfCell;
}


}
