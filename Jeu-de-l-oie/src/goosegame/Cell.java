package goosegame;

import goosegame.*;

public  class Cell {
  protected Player player;
  protected int index;

/** Create a cell at a certain position on the board
* @param index the position of the cell on the board
*/
public Cell(int index){
  this.index= index ;
  this.player= null;
}

/**
*return the position of the cell on the board
* @return the position of the cell on the board
*/
public int getIndex(){
  return this.index;
}

/**
* verify if this cell can be left
* @return true if this cell is not a trap cell,false otherwise
*/
public  boolean canBeLeft(){
return true;
}
 /**
 * verify if this cell has a player
 * @return true if this cell has a player ,false otherwise
 */
public boolean isOccupied(){
  return this.player !=null;
}

/**
*return the player in this cell
* @return a player who is in this cell
*/
public Player getPlayer(){
  return this.player;
}

/**
*change the player in this cell
* @param p a new player for this cell
*/
public void setPlayer(Player p){
  this.player = p;
}

/** return the index of the destination cell
* @param d the number the player threw with the dices
* @return the index of this cell
*/
public int bounce(int d){
  return this.index;
  }

/** display the state of this cell(means the position of this cell on the board)
* @return a string of this cell's state
*/
  public String toString(){
    return " cell " +this.index;
  }
}
