package goosegame;
import goosegame.*;

import java.util.*;

/**
 * A goose game defined by a board.
 *
 * @author Khaled Altayeb Alrafei & Abdelziz HAMMAD AHMED
 * @version 1.0
 */

public class Game{

	/* the class's attributes*/

	/*the list of players */
	protected List<Player> thePlayers;

	/*the board of the game */
	protected Board board;


	/**
	* Creates a new game
	* @param board the board of the game
	*/
	public Game(Board board){
		this.board=board;
		this.thePlayers= new ArrayList<Player>();
	}

	/**
	* Add a player in the game
	* @param p a player to add
	*/
	public void addPlayer(Player p){
		Cell c = this.board.getCell(0);
		p.setCell(c);
		this.thePlayers.add(p);
	}

	/**
	* Play the game "jeu de l'oie"
	*/
	public void play(){
		int d;
		Cell currentCell,reboundCell;
		String name;
	  while (true){
		Player	p = this.nextPlayer();
		name = p.toString();
		currentCell = p.getCell();
		System.out.print(" " + name + " is in " + currentCell.toString() + " ,");
			if(this.canPlay(p)){
					d = p.twoDiceThrow();
					int reached = this.limit_cell(currentCell.getIndex() + d);
					Cell reachedCell = this.board.getCell(reached);
					System.out.print(" " + name + " throws " + d + " and reaches" + reachedCell.toString() + " ");
					if(reachedCell instanceof GooseCell) System.out.print(" and jumps to");
					int realIndexReachedCell = this.limit_cell(reachedCell.bounce(d));
					if(this.board.getCell(realIndexReachedCell) instanceof TeleportCell){
						int destIndex = reachedCell.bounce(d);
						reboundCell = this.board.getCell(destIndex);
					}
					else{
					reboundCell = this.board.getCell(realIndexReachedCell);
				}
					if(reboundCell != reachedCell) System.out.println(" "+reboundCell.toString());
					System.out.println();
					if(reboundCell.isOccupied()){
						System.out.print(" " + reboundCell.toString() + " is busy, ");
						Player p2 = reboundCell.getPlayer();
							this.swap(p,p2);
						System.out.println(p2.toString()+ " is sent to " + currentCell.toString());
						}
			else{
				p.getCell().setPlayer(null);
				p.setCell(reboundCell);
				reboundCell.setPlayer(p);
			}
		}
		else{
		System.out.println(name + " cannot play. ");
	}
		if(this.isFinished()){
			    return;
	}
}


}


	/**
	* Test if this game is finished
	* @return true if the game is finished ,otherwise false.
	*/
	public boolean isFinished(){
			Cell lastCell = this.board.getCell(this.board.getNbOfCells());
			Player p = lastCell.getPlayer();
	    if(p != null){
		System.out.println(" " + p.toString()+ " has won.");
		return true;
	    }
    	return false;
	}

	/**
	* Verify if a player given in parameter can play
	* @param p the player
	* @return true if the player can play,false otherwise
	*/
		public boolean canPlay(Player p){
			return p.getCell().canBeLeft();
		}

		/**
		* swap the position of two players given in parameter on the board
		* @param p1 the first player
		* @param p2 the second player
		*/
		public void swap(Player p1,Player p2){
			Cell tmp = p1.getCell();
			Cell tmp2 = p2.getCell();
			tmp.setPlayer(p2);
			tmp2.setPlayer(p1);
			p1.setCell(tmp2);
			p2.setCell(tmp);
		}

		/**
		* return the right position on the board if a player exceed the number of cells(is outside of the board)
		* @param n the sum of the position of a player on the board and the number of dice
		* @return a integer,a position on the board
		*/
			public int limit_cell(int n){
				int diff = n - this.board.getNbOfCells();
				return this.board.getNbOfCells() - Math.abs(diff);
			}

/*the index of the next player */
private static int i = 0;

		/**return the next player to play
		* @return a player
		*/
			public Player nextPlayer(){
				int d;
				int n = this.thePlayers.size();
				d = i;
				i = (i+1)%n;
				return this.thePlayers.get(d);
			}


}
