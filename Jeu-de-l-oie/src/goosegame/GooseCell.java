package goosegame;

import goosegame.*;

/**
 * A particular cell(GooseCell) of the goose game
 *
 * @author Khaled Altayeb Alrafei & Abdelziz HAMMAD AHMED
 * @version 1.0
 */

// CONSTRUCTEUR

public class GooseCell extends Cell{
  public GooseCell(int index){
    super(index);
  }

// METHODE

  /** return the index of the destination cell
  * @param d the number the player threw with the dices
  * @return the index of the next cell where the player bounce
  */
  public int bounce(int d){
    return this.index+d;
  }

  /** display the state of the cell(means this cell's position on the board )
  * @return the string of this cell's state
  */
  public String toString(){
		return super.toString()+" (goose) ";
	}
}
