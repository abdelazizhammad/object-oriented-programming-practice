package goosegame;

import goosegame.*;

/**
 * An interactive goose game defined by the number of players.
 *
 * @author Khaled Altayeb Alrafei & Abdelziz HAMMAD AHMED
 * @version 1.0
 */

 public class Main{
   public static void main(String [] argv){
     if(argv.length!=1){
      System.out.println("Usage: java goosegame.Main <nbPlayers>");
      System.exit(1);
    }
    Board b = new BasicBoard();
    b.initBoard();
    Game g = new Game(b);
    int nbPlayers = Integer.parseInt(argv[0]);
    for (int t = 0;t < nbPlayers;t++){
      String namePlayer = "Joueur "+(t+1);
      g.addPlayer(new Player(namePlayer));
    }
    g.play();
 }
}
