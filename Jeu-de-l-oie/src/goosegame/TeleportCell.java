package goosegame;
import goosegame.*;


/**
 * A particular cell(TeleportCell) of the goose game
 *
 * @author Khaled Altayeb Alrafei & Abdelziz HAMMAD AHMED
 * @version 1.0
 */


public class TeleportCell extends Cell{
  /* the index of the cell where to teleport the player*/
  protected int dest;

/** Creates a teleport cell
* @param index the position of the cell on the board
* @param dest the position where the player will be sent
*/
public TeleportCell(int index, int dest){
  super(index);
  this.dest = dest;
}

/** return the index of the next cell where to teleport the player
* @return an integer,the index of the destinatioon cell
*/
public int bounce(int d){
  return this.dest;
}

/** display the state of the cell(means the position of the cell and where the player is sent)
* @return a string of the state of the cell
*/
public String toString(){
  return super.toString() + " (teleport to "+ this.dest + ") and jumps to ";
}
}
