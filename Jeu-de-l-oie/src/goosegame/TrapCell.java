package goosegame;

import goosegame.*;


/**
 * A particular cell(TrapCell) of the goose game
 *
 * @author Khaled Altayeb Alrafei & Abdelziz HAMMAD AHMED
 * @version 1.0
 */

// CONSTRUCTEUR

public class TrapCell extends Cell{
  public TrapCell(int index){
    super(index);
  }


// METHODE

/** verify if a the player on the cell can leave this cell
* @return true if the player can leave the cell,false otherwise
*/
  public boolean canBeLeft(){
    return false;
  }

  /** display the state of this cell(means the position of this cell on the board)
  * @return a string of this cell's state
  */
  public String toString(){
		return super.toString()+" (trap)";
	}
}
