package goosegame;

import goosegame.*;

/**
 * A particular cell(WaitingCell) of the goose game
 *
 * @author Khaled Altayeb Alrafei & Abdelziz HAMMAD AHMED
 * @version 1.0
 */

public class WaitingCell extends Cell{

  // ATTRIBUTS

  /* the time to wait in this cell */
  protected final int WaitingTime;

  /* the number of tours before a player trapped in this cell can left */
  protected int nbTours;


  // CONSTRUCTEUR

/** a waiting time given in parameter
* @param index the position of the cell on the board
* @param waitingtime the number of tours the player will have to wait so that he can play again
*/
  public WaitingCell(int index,int waitingtime){
    super(index);
    this.WaitingTime = waitingtime;
  }

// METHODES

  /**
  * verify if this cell can be left
  * @return true if the player can leave,false otherwise
  */

  public boolean canBeLeft(){
    if (nbTours==0){
      return true;
    }
    this.nbTours--;
    return false;
  }

/** Change the player in this cell ,if the player is changed, the waitingtime is reset
* @param p a player to change with the actual player in this cell
*/
    public void setPlayer(Player p){
      this.nbTours = this.WaitingTime;
      super.setPlayer(p);
    }

/** display the state of the cell(means the position of the cell on the board and the time to wait in this cell)
* @return the string of this cell's state
*/
    public String toString(){
      if(nbTours == 0) return super.toString();
      return super.toString() + "(waiting for "+this.nbTours +" turns)";
    }

}
