package goosegame;
import goosegame.*;
import java.util.*;

public class ZeroCell extends Cell{
  protected int index;
  private List<Player> players;


  public ZeroCell() {
    super(0);
    this.players=new ArrayList<Player>();

  }

  public  void addPlayer(Player p){
    this.players.add(p);

  }

  public int getIndex(){
		return 0;
	}

  /** display the state of the cell(means the position of the cell on the board )
  * @return the string of this cell's state
  */
	public String toString(){
		return "cell 0";
	}

/** tells whether this cell can be left by a player
* @return true if a player on this cell can leave,false otherwise
*/
	public boolean canBeLeft(){
		return true;
	}

  /** tells whether this cell has players on it
  * @return always false because this cell can accept many players 
  */
	public boolean isOccupied(){
		return false;
	}

}
