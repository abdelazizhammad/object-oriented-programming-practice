package goosegame;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class BasicBoardTest {


	@Test
	public void createTest() {
		BasicBoard db = new BasicBoard();
		assertNotNull(db);
	}


    // ---Pour permettre l'execution des tests ----------------------
    public static junit.framework.Test suite() {
	return new junit.framework.JUnit4TestAdapter(goosegame.BasicBoardTest.class);
    }

}
