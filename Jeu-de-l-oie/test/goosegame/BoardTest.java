package goosegame;
import goosegame.*;

import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

public class BoardTest{


@Test
public void boardTest(){
  Board board=new BasicBoard();
  assertNotNull(board);
}


@Test
public void getNbOfCellsTest(){
    Board board=new BasicBoard();
    assertEquals(63,board.getNbOfCells());
}


  @Test
  public void testGetCell() {
    Board board=new BasicBoard();
    assertNotNull(board.getCell(4));
  }



// ---Pour permettre l'exécution des test----------------------
   public static junit.framework.Test suite() {
       return new junit.framework.JUnit4TestAdapter(goosegame.BoardTest.class);
   }
}
