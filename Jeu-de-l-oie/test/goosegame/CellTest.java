package goosegame;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class CellTest {

  private Cell a;
  private Player p;

    @Before
    public void before() {
      this.a = new Cell(5);
      this.p = new Player("Sam");
    }



	@Test
	public void createTest() {
		assertNotNull(this.a);
	}

	@Test
	public void testIndex() {
		assertEquals(this.a.getIndex(), 5);
	}

	@Test
	public void testCanBeLeft() {
		assertTrue(this.a.canBeLeft());
	}

	@Test
	public void testIsOccupied() {
		assertFalse(this.a.isOccupied());
		this.a.setPlayer(this.p);
		assertTrue(this.a.isOccupied());
	}


  @Test
  public void testBounce() {
    assertEquals(this.a.bounce(4),5);
}

@Test
public void testSetPlayer() {
  this.a.setPlayer(this.p);
  assertEquals(this.p,this.a.getPlayer());
}

  @Test
  public void testgetPlayer() {
    this.a.setPlayer(this.p);
    assertEquals(this.p,this.a.getPlayer());
  }

    // ---Pour permettre l'execution des tests ----------------------
    public static junit.framework.Test suite() {
	return new junit.framework.JUnit4TestAdapter(goosegame.CellTest.class);
    }

}
