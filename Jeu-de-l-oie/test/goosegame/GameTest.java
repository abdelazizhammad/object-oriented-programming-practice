package goosegame;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class GameTest {

  private Game g;
  private Board b;
  private Player p1;
  private Player p2;

    @Before
    public void before() {
      this.p1 = new Player("Sam");
      this.p2 = new Player("bob");
      this.b = new BasicBoard();
      this.b.initBoard();
      this.g = new Game(this.b);
    }

    @Test
    public void createTest(){
      assertNotNull(this.g);
    }

    @Test
    public void testLimitCell(){
      assertEquals(this.g.limit_cell(67),59);
    }

    @Test
    public void testNextPlayer(){
      this.g.addPlayer(this.p1);
      this.g.addPlayer(this.p2);
      assertSame(this.p1,this.g.nextPlayer());
    }

    @Test
    public void testCanPlay(){
      this.g.addPlayer(this.p1);
      assertTrue(this.g.canPlay(this.p1));
    }

    // ---Pour permettre l'execution des tests ----------------------
        public static junit.framework.Test suite() {
    	return new junit.framework.JUnit4TestAdapter(goosegame.GameTest.class);
        }

    }
