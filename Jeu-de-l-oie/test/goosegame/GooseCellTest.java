package goosegame;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class GooseCellTest {

  private GooseCell c;
  private Player p;

  @Before
  public void before() {
    this.c = new GooseCell(9);
    this.p = new Player("Pablo");
  }



	@Test
	public void createTest() {
		assertNotNull(this.c);
	}

  @Test
  public void testBounce() {
    assertEquals(19,this.c.bounce(10));
  }



    // ---Pour permettre l'execution des tests ----------------------
    public static junit.framework.Test suite() {
	return new junit.framework.JUnit4TestAdapter(goosegame.GooseCellTest.class);
    }

}
