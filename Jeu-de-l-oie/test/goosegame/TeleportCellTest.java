package goosegame;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class TeleportCellTest {

	@Test
	public void createTest() {
		Cell c = new TeleportCell(58, 1);
		assertNotNull(c);
	}

	@Test
	public void testBounce() {
		Cell c = new TeleportCell(58, 1);
		assertEquals(c.bounce(4), 1);
	}



    // ---Pour permettre l'execution des tests ----------------------
    public static junit.framework.Test suite() {
	return new junit.framework.JUnit4TestAdapter(goosegame.TeleportCellTest.class);
    }

}
