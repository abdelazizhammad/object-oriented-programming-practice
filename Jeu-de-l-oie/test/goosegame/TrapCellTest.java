package goosegame;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class TrapCellTest {


	@Test
	public void createTest() {
		Cell c = new TrapCell(9);
		assertNotNull(c);
	}


	@Test
	public void testCanBeLeft() {
		Cell c = new TrapCell(9);
		assertFalse(c.canBeLeft());
	}

	
    // ---Pour permettre l'execution des tests ----------------------
    public static junit.framework.Test suite() {
	return new junit.framework.JUnit4TestAdapter(goosegame.TrapCellTest.class);
    }

}
