package goosegame;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class ZeroCellTest {

  private ZeroCell c;
  private Player p;

  @Before
  public void before() {
    this.c = new ZeroCell();
    this.p = new Player("Pablo");
  }

	@Test
	public void createTest() {
		assertNotNull(this.c);
	}

	@Test
	public void testIndex() {
		assertEquals(this.c.getIndex(), 0);
	}

  @Test
  public void testCanBeLeft() {
    assertTrue(this.c.canBeLeft());
  }


  @Test
    public void testIsOccupied() {
      assertFalse(this.c.isOccupied());
    }


    // ---Pour permettre l'execution des tests ----------------------
    public static junit.framework.Test suite() {
	return new junit.framework.JUnit4TestAdapter(goosegame.ZeroCellTest.class);
    }

}
