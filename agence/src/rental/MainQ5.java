package rental;

import java.util.*;
import rental.*;
import rental.heritages.*;


/** main to test for question  Q5 */
public class MainQ5 {

  public static void main(String[] args) throws Exception {
	RentalAgency agency = new RentalAgency();
	Vehicle v = new Vehicle("Ford","X111",2016,100);
  Car c = new Car("Peugeot","F56",2014,145,7);
  Motorbike m = new Motorbike("Yamaha","D45",2017,50,750);
	agency.addVehicle(v);
  agency.addVehicle(c);
  agency.addVehicle(m);
  PriceCriterion p = new PriceCriterion(100);
	agency.displaySelection(p);
}
}
