package rental;

import java.util.*;
import rental.*;
import rental.exceptions.*;

/** a rental vehicle agency, client can rent one vehicle at a time */
public class RentalAgency {
    // vehicles of this agency
    private List<Vehicle> theVehicles;

    // maps client and rented vehicle (at most one vehicle by client)
    private Map<Client,Vehicle> rentedVehicles;

    public RentalAgency() {
    	this.theVehicles= new ArrayList<Vehicle>();
      this.rentedVehicles = new HashMap<Client,Vehicle>();
    }

    /** adds a vehicle to this agency
    * @param v the added vehicle
    */
    public void addVehicle(Vehicle v) {
      this.theVehicles.add(v);

    }

    /** provides the list of the vehicles that satisfy the criterion c
    * @param c the selection criterion
    * @return  the list of the vehicles that satisfy the criterion c
    */
    public List<Vehicle> select(Criterion c) {
    	List<Vehicle> result = new ArrayList<Vehicle>();
      for (Vehicle v: this.theVehicles){
        if (c.isSatisfiedBy(v)){
          result.add(v);
        }
      }
        return (result);
    }
    /** displays the vehicles that satisfy the criterion c
    * @param c the selection criterion
    */
    public void displaySelection(Criterion c) {
    	List<Vehicle> result = this.select(c);
      for (Vehicle v:result){
        System.out.println(v+"\n");
      }
    }

    /** client rents a vehicle
    * @param client the renter
    * @param v the rented vehicle
    * @return the daily rental price
    * @exception UnknownVehicleException   if v is not a vehicle of this agency
    * @exception IllegalStateException if v is already rented or client rents already another vehicle
    */
    public float rentVehicle(Client client, Vehicle v) throws UnknownVehicleException, IllegalStateException {
      if(this.theVehicles.indexOf(v) != -1){
        if(! isRented(v) || ! hasRentedAVehicle(client)){
          this.rentedVehicles.put(client,v);
        }
        else{
          throw new IllegalStateException();
        }
      }
      else{
        throw new UnknownVehicleException();
      }
      return v.getDailyPrice();
    }

    /** returns <em>true</em> iff client c is renting a vehicle
    * @param client the person who possibly rent a vehicle
    * @return <em>true</em> iff client c is renting a vehicle
    */
    public boolean hasRentedAVehicle(Client client){
      for(Client f : this.rentedVehicles.keySet()){
        if (client.equals(f))
          return true;
      }
      return false;
      //client.equals
    	//return this.rentedVehicles.containsKey(client);
    }

    /** returns <em>true</em> iff vehicle v is rented
    * @param v the vehicle to check if it is rented 
    * @return <em>true</em> iff vehicle v is rented
    */
    public boolean isRented(Vehicle v){
      if(this.rentedVehicles.containsValue(v))
        return true;
      return false;
    }

    /** the client returns a rented vehicle. Nothing happens if client didn't have rented a vehicle.
    * @param client the client who returns a vehicle
    */
    public void returnVehicle(Client client){
    	this.rentedVehicles.remove(client);
    }
    /** provides the collection of rented vehicles for this agency
    * @return collection of currently rented vehicles
    */
    public Collection<Vehicle> allRentedVehicles(){
    	return this.rentedVehicles.values();
    }

}
