package rental;

import rental.*;
import rental.exceptions.*;
import rental.heritages.*;

public class SuspiciousRentalAgency extends RentalAgency{
  public SuspiciousRentalAgency(){
    super();
  }

  public float rentVehicle(Client c,Vehicle v) throws UnknownVehicleException, IllegalStateException {
    if (c.getAge() < 25){
    return  super.rentVehicle(c,v) + ((super.rentVehicle(c,v) * 10)/100);
    }
    return super.rentVehicle(c,v);
  }

}
