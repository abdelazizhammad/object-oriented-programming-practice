package rental.exceptions;

public class UnknownVehicleException extends Exception {

	public UnknownVehicleException() {
	}

	public UnknownVehicleException(String msg) {
		super(msg);
	}
}
