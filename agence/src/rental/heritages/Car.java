package rental.heritages;

import rental.*;

public class Car extends Vehicle{

  //ATTRIBUTS

  //number of Passengers
  private int passengers;

  // CONSTRUCTEUR

  /**
	 * creates a car with given informations
	 *
	 * @param brand
	 *            the vehicle's brand
	 * @param model
	 *            the vehicle's model
	 * @param productionYear
	 *            the vehicle's production year
	 * @param dailyRentalPrice
	 *            the daily rental price
       	 * @param n
	 *		the number of passengers	 
	 */
  public Car(String brand, String model, int productionYear, float dailyRentalPrice, int n){
    super(brand, model, productionYear, dailyRentalPrice);
    this.passengers = n;
  }

  //METHODES

  /**
	 * @return this vehicle's number of passengers that can take
	 */
  public int getNbPassengers(){
    return this.passengers;
  }

  /**
	* returns informations of this vehicule
	* @return informations of this vehicule
	*/
  public String toString(){
    return super.toString() + "à "+this.passengers + " passagers.";
  }
}
