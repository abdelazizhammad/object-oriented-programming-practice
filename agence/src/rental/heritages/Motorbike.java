package rental.heritages;

import rental.*;

public class Motorbike extends Vehicle{

  //ATTRIBUTS

  //the cylinder of this motorbike
  private int cylinder;

  // CONSTRUCTEUR

  /**
	 * creates a motorbike with given informations
	 *
	 * @param brand
	 *            the vehicle's brand
	 * @param model
	 *            the vehicle's model
	 * @param productionYear
	 *            the vehicle's production year
	 * @param dailyRentalPrice
	 *            the daily rental price
       	 * @param cylinder
	 *		the volume of the cylinder	 
	 */
  public Motorbike(String brand, String model, int productionYear, float dailyRentalPrice,int cylinder){
    super(brand,model,productionYear,dailyRentalPrice);
    this.cylinder = cylinder;
  }

  //METHODES

  /**
	 * @return the volume of this motorbike's cylinder
	 */
  public int getCylinder(){
    return this.cylinder;
  }

/**
	* returns informations of this motorbike
	* @return informations of this motorbike
	*/
  public String toString(){
    return super.toString() + this.cylinder + "cm3";
  }

}
