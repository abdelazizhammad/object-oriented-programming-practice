package rental;

import static org.junit.Assert.*;
import rental.heritages.*;
import org.junit.*;

public class CarTest {

	private Car c1;
	private Car c2;


	@Before
	public void before() {
		this.c1 = new Car("brand1","model1",2015,100.0f,2);
		this.c2 = new Car("brand2","model2",2000,200.0f,4);
	}

	@Test
	public void createTest() {
		assertNotNull(this.c1);
	}

  @Test
	public void getNbPassengers(){
    assertEquals(2,this.c1.getNbPassengers());
    assertEquals(4,this.c2.getNbPassengers());
  }

  // ---Pour permettre l'execution des tests ----------------------
public static junit.framework.Test suite() {
  return new junit.framework.JUnit4TestAdapter(rental.CarTest.class);
}

}
