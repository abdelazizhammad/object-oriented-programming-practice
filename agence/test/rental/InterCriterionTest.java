package rental;

import static org.junit.Assert.*;
import org.junit.*;
import rental.*;

public class InterCriterionTest {


  @Test
	public void testInterCriterion() {
		assertNotNull(new InterCriterion());
	}

	@Test
	public void testIsSatisfiedBy() {
    Vehicle v1 = new Vehicle("brand1","model1",2015,100);
    Vehicle v2 = new Vehicle("brand2","model1",2016,150);
    Vehicle v3 = new Vehicle("brand1","model2",2016,120);
    Vehicle v4 = new Vehicle("brand3","model1",2016,180);


    Criterion pc = new PriceCriterion(150);
    Criterion bc = new BrandCriterion("brand1");
    InterCriterion ic = new InterCriterion();
    ic.addCriterion(pc);
    ic.addCriterion(bc);
    assertSame(ic.isSatisfiedBy(v1), pc.isSatisfiedBy(v1) && bc.isSatisfiedBy(v1));
    assertSame(ic.isSatisfiedBy(v2), pc.isSatisfiedBy(v2) && bc.isSatisfiedBy(v2));
    assertSame(ic.isSatisfiedBy(v3), pc.isSatisfiedBy(v3) && bc.isSatisfiedBy(v3));
    assertSame(ic.isSatisfiedBy(v4), pc.isSatisfiedBy(v4) && bc.isSatisfiedBy(v4));
	}


    // ---Pour permettre l'execution des tests ----------------------

  public static junit.framework.Test suite() {
    return new junit.framework.JUnit4TestAdapter(rental.InterCriterionTest.class);
  }

}
