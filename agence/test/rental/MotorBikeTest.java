package rental;

import static org.junit.Assert.*;
import rental.*;
import rental.heritages.*;
import org.junit.*;

public class MotorBikeTest {

	private Motorbike mo1;
	private Motorbike mo2;


	@Before
	public void before() {
		this.mo1 = new Motorbike("brand1","model1",2015,100.0f,456);
		this.mo2 = new Motorbike("brand2","model2",2000,200.0f,765);
	}

	@Test
	public void createTest() {
		assertNotNull(this.mo1);
	}

  @Test
  public void getCylinder(){
    assertEquals(456,this.mo1.getCylinder());
    assertEquals(765,this.mo2.getCylinder());

  }

// ---Pour permettre l'execution des tests ----------------------
public static junit.framework.Test suite() {
  return new junit.framework.JUnit4TestAdapter(rental.MotorBikeTest.class);
}

}
