package rental;
 
import org.junit.*;
import static org.junit.Assert.*;
import rental.*;
import rental.exceptions.*;

public class RentalAgencyTest {

  @Test
  public void testCreation(){
    assertNotNull(new RentalAgency());
  }

  private RentalAgency ra;
  private Vehicle v1;
  private Vehicle v2;
  private Vehicle v3;
  private Vehicle v4;
  private Client c1;
  private Client c2;
  private Client c3;
  private Client c4;

 @Before
 	public void before() {
    this.ra = new RentalAgency();
    this.v1 = new Vehicle("b", "m", 1, 5.0f);
    this.v2 = new Vehicle("c", "m", 1, 6.5f);
    this.v3 = new Vehicle("d", "m", 1, 6.5f);
    this.v4 = new Vehicle("e", "m", 1, 6.5f);
    this.c1 = new Client("nom",20);
    this.c2 = new Client("nomPrenom",20);
    this.c3 = new Client("nomEtPrenom",20);
    this.c4 = new Client("nomPlusPrenom",20);
}
 @Test
   public void testRentVehicleOK() throws Exception{
       this.ra.addVehicle(this.v1);
       this.ra.addVehicle(this.v2);
       assertEquals(this.ra.rentVehicle(this.c1,this.v1), 5.0f, 0.00001);
       assertEquals(this.ra.rentVehicle(this.c2,this.v2), 6.5f, 0.00001);
   }
   @Test(expected=UnknownVehicleException.class)
   public void testRentVehicleUnknown() throws Exception{
     this.ra.rentVehicle(this.c1,this.v1);
 }
 
@Test
public void testIsRented() throws Exception{
  this.ra.addVehicle(this.v1);
  this.ra.addVehicle(this.v2);
  this.ra.addVehicle(this.v3);
  this.ra.addVehicle(this.v4);
  assertEquals(this.ra.rentVehicle(this.c1,this.v1), 5.0f, 0.00001);
  assertEquals(this.ra.rentVehicle(this.c2,this.v2), 6.5f, 0.00001);
  assertTrue(this.ra.isRented(this.v1));
  assertTrue(this.ra.isRented(this.v2));
  assertFalse(this.ra.isRented(this.v3));
  assertFalse(this.ra.isRented(this.v4));
}
@Test
public void testHasRentedAVehicle() throws Exception{
  this.ra.addVehicle(this.v1);
  this.ra.addVehicle(this.v2);
  assertEquals(this.ra.rentVehicle(this.c1,this.v1), 5.0f, 0.00001);
  assertEquals(this.ra.rentVehicle(this.c2,this.v2), 6.5f, 0.00001);
  assertTrue(this.ra.hasRentedAVehicle(this.c1));
  assertTrue(ra.hasRentedAVehicle(this.c2));
  assertFalse(ra.hasRentedAVehicle(this.c3));
  assertFalse(ra.hasRentedAVehicle(this.c4));
}

@Test
public void testReturnVehicle() throws Exception{
  this.ra.addVehicle(this.v1);
  this.ra.addVehicle(this.v2);
  assertEquals(this.ra.rentVehicle(this.c1,this.v1), 5.0f, 0.00001);
  assertEquals(this.ra.rentVehicle(this.c2,this.v2), 6.5f, 0.00001);

  assertTrue(this.ra.isRented(this.v1));
  assertTrue(this.ra.isRented(this.v2));
  assertTrue(this.ra.hasRentedAVehicle(this.c1));
  assertTrue(this.ra.hasRentedAVehicle(this.c2));
  assertEquals(this.ra.allRentedVehicles().size(), 2);

  this.ra.returnVehicle(this.c1);

  assertFalse(this.ra.isRented(this.v1));
  assertTrue(this.ra.isRented(this.v2));
  assertFalse(this.ra.hasRentedAVehicle(this.c1));
  assertTrue(this.ra.hasRentedAVehicle(this.c2));
  assertEquals(this.ra.allRentedVehicles().size(), 1);
}

@Test
public void testReturnUnRentedVehicle() throws Exception{
  this.ra.addVehicle(this.v1);

  assertFalse(this.ra.hasRentedAVehicle(this.c1));
  assertFalse(this.ra.hasRentedAVehicle(this.c2));
  assertFalse(this.ra.isRented(this.v1));
  assertFalse(this.ra.isRented(this.v2));
  assertEquals(this.ra.allRentedVehicles().size(), 0);

  this.ra.returnVehicle(this.c1);
  this.ra.returnVehicle(this.c2);
}


// ---Pour permettre l'execution des tests ----------------------

public static junit.framework.Test suite() {
return new junit.framework.JUnit4TestAdapter(rental.RentalAgencyTest.class);
}

}
