package rental;

import static org.junit.Assert.*;

import org.junit.*;
import rental.*;
import rental.exceptions.*;
import rental.heritages.*;

public class SuspiciousRentalAgencyTest {

  private SuspiciousRentalAgency ra;
  private Vehicle v1;
  private Vehicle v2;
  private Vehicle v3;
  private Vehicle v4;
  private Client c1;
  private Client c2;
  private Client c3;

  @Before
  public void before() {
    this.ra = new SuspiciousRentalAgency();
    this.v1 = new Vehicle("b", "m", 1, 110);
    this.v2 = new Vehicle("c", "m", 1, 100);
    this.v3 = new Vehicle("b", "m", 1, 5.0f);
    this.c3 = new Client("nom",28);
    this.c1 = new Client("nom",20);
    this.c2 = new Client("nomPrenom",25);
}

  @Test
  public void testCreation(){
    assertNotNull(new RentalAgency());
  }

  @Test
  public void testRentVehicleOK() throws Exception{
    this.ra.addVehicle(this.v1);
    this.ra.addVehicle(this.v2);
    assertEquals(this.ra.rentVehicle(this.c1,this.v1), 110, 0.00001);
    assertEquals(ra.rentVehicle(this.c2,this.v2), 100, 0.00001);
  }

  @Test(expected=UnknownVehicleException.class)
  public void testRentVehicleUnknown() throws Exception{
    this.ra.rentVehicle(this.c3,this.v3);
  }



  @Test(expected=IllegalStateException.class)
  public void testRentVehicleIllegal1() throws Exception{
    RentalAgency ra = new RentalAgency();
    Vehicle v1 = new Vehicle("b", "m", 1, 5.0f);
    ra.addVehicle(v1);
    Client c1 = new Client("nom", 30);
    Client c2 = new Client("nomPrenom", 30);
    assertEquals(ra.rentVehicle(c1,v1), 5.0f, 0.00001);
    ra.rentVehicle(c2,v1);
  }


  @Test(expected=IllegalStateException.class)
  public void testRentVehicleIllegal2() throws Exception{
    RentalAgency ra = new RentalAgency();
    Vehicle v1 = new Vehicle("b", "m", 1, 5.0f);
    Vehicle v2 = new Vehicle("c", "m", 1, 6.5f);
    ra.addVehicle(v1);
    ra.addVehicle(v2);
    Client c1 = new Client("nom",30);
    assertEquals(ra.rentVehicle(c1,v1), 5.0f, 0.00001);
    ra.rentVehicle(c1,v2);
  }



    // ---Pour permettre l'execution des tests ----------------------

  public static junit.framework.Test suite() {
    return new junit.framework.JUnit4TestAdapter(rental.SuspiciousRentalAgencyTest.class);
  }

}
