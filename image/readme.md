Abdelaziz & Khaled


Objectifs
==========

Ce TP avait pour objectif de se familiariser avec les paquetages et la méthodologie de travail (signature, doc, tests, implantation) et les interfaces.

Problème rencontré
===================

L'execution de ImageMain renvoi une exception.Pas de scan sur l'image mis en paramètre.

La Documentation
=================

Pour générer la documentation il faut se placer dans le répertoire src et exécuter la commande `javadoc image image.color -d ../docs`.


Compilation des Classes
=======================

Le répertoire "classes" va contenir plusieurs fichiers .class produit lors de la compilation :

* ImageMain.java : Compiler grâce à la commande `javac image/ImageMain.java -d ../classes`  depuis le répertoire src

* ImageExample.java : Compiler grâce à la commande `javac image/ImageExample.java -d ../classes`  depuis le répertoire src

* GrayColor.java : Compiler grâce à la commande `javac image/color/GrayColor.java -d ../classes`  depuis le répertoire src

* Pixel.java : Compiler grâce à la commande `javac image/Pixel.java -d ../classes`  depuis le répertoire src

Compilation et execution des Tests
==================================

* GrayColor.java :depuis le répertoire racine image

    * Compiler grâce à la commande `javac -classpath test-1.7.jar test/GrayColorTest.java`

    * Lancer grâce à la commande `java -jar test-1.7.jar GrayColorTest`

* Pixel.java :

    * Compiler grâce à la commande `javac -classpath test-1.7.jar test/PixelTest.java`

    * Lancer grâce à la commande `java -jar test-1.7.jar PixelTest`

* Image.java :

    * Compiler grâce à la commande `javac -classpath test-1.7.jar test/ImageTest.java`

    * Lancer grâce à la commande `java -jar test-1.7.jar ImageTest`

Exécution des archives
=======================

Se placer dans le répertoire contenant l'archive (la racine du projet) et exécuter la commande:

	* `java -jar imageexample.jar <threshold>` où `<threshold>` est le seuil pour détecter les contours.

	* `java -jar imagemain.jar <threshold> <nbOfGrayLevels>` où `<threshold>` est le seuil pour détecter les contours, et `<nbOfGrayLevels>` est le nombre de niveaux de gris permis.
