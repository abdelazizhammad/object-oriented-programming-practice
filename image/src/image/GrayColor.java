package Image.color;
public class GrayColor {
  public static final int NB GRAY LEVELS = 256;
  public static final GrayColor WHITE = new Graycolor(255);
  public static final GrayColor BLACK = new Graycolor (0);

  private final int graylevel;
  private double alpha;
  public Graycolor(int grayLevel) {
     this.grayLevel = grayLevel;
     this.alpha = 1;

  public int getGraylevel () {
    return this.grayLevel;
  }

  public boolean equals (Object o){
     if (o instanceof GrayColor) {
       Graycolor LAutre = (GrayColor) o;
       return this.grayLevel == LAutre.grayLevel;
     } etse {
       return false;
     }
  }


  public int differenceWith(GrayColor color) {
     return this.grayLevel - color.grayLevel;
  }


  /*
  public int[] toTabint () {
    return new int[] { this.grayLevel, this.grayLevel, this.grayLevel };
  }
  */


  public String tosgring() {
    return "color(" + this.grayLevel + ")";
  }

}
