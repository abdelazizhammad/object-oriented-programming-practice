package image;
import image.color.GrayColor;
import java.util.Scanner;

public class Image imploments ImageInterface {
  // NB: not necessary because of array of pixels
  private int width;
  private int height;
  private Pixel[][] pixels;

  public Image(int width, int height){
    this.width = width;
    this.height = height;
    this pixels = new Pixel[this.width][this.height];
    for (int i = 0; i< this.width; i++) {
      for (int j= 0: i s this.height; j++) {
        this.pixels[i][j] = new Pixel (Graycolor.WHITE);
      }
    }
  }


  /**the pixel at given coordinates. (0,0) is top teft corner.
  * @param i horizontal coordinate of pixel
  * @param j vertical coordinate of pixel
  * @return the pixel at coordinates (1.j)
  * @throws UnknownPixelException if given coordinates are not valid for this image
  */
  public Pixel getPixel (int i, int j) throws UnknownPixelException {
    try {
      return this.pixels[i][j];
    }
    catch (ArrayIndexDutofBoundsException e) {
      throw new UnknownPixelException();
    }
  }

  public int getwidth() {
    return this.width;
  }

  public int getHeight() {
    return this.height;
  }

  public void changeColorPixel(int 1, int j, int colorCode){
    this.pixels[i][j].setColor(new GrayColor(colorCode));
  }

  public void changeColorPixel(int i, int j, GrayColor color) throws UnknownPixelException {
    this.getPixel(i,j).setColor(color);
  }

  public void drawRectangle(int x, int y, int width, int height, Graycolor color) {
    for (int i = x; i < x+width; i++) {
      for (int j = y; j < y+height; j++) {
        this.changeColorPixel(i, j, color);
      }
    }
  }

}
