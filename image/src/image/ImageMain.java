package image;

import image.Image;
import image.ImageDisplayer;

/**
* @author Abdelaziz & Khaled
*/

public class ImageMain{

  public static void main(String args[]){
    if(args.length!=3){
      System.out.println("Usage: java image.ImageMain <filename> <threshold> <nbOfGrayLevels>");
      System.exit(1);
    }
    int threshold=0;
    int nbLevels=1;
    try{
      threshold=Integer.parseInt(args[1]);
      nbLevels=Integer.parseInt(args[2]);
    }
    catch(NumberFormatException e){
      System.out.println("the threshold and the number of gray levels must be numbers");
      System.exit(1);
    }
    Image image = Image.initImagePGM(args[0]);
    ImageDisplayer imgDisp = new ImageDisplayer();
    imgDisp.display(image, "Original");
    imgDisp.display(image.edge(threshold), "Contours");
    imgDisp.display(image.decreaseNbGrayLevels(nbLevels), "Decreased Gray Levels");
  }


}
