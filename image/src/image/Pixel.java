package image;
import image.color.GrayColor;
public class Pixel {
  private Graycolor color;

  public Pixel(GrayColor c){
    this.color = c;
  }

  public Graycolor getColor(){
    return this.color;
  }


  public void setColor(GrayColor c) {
    this.color = c;
  }

  public int colorLevelDifference (Pixel pixel) {
    return Math.abs (this.color.differenceWith(pixel.color));
  }

  public boolean equals (Object o) {
    if (o instanceof Pixel){
      Pixel LAutre = (Pixel) o;
      return this. color.equals(LAutre, color);
    } else {
      return false;
    }
  }

  public String tostring(){
    return "pix(" +this.color+ ")";
  }

}
    
