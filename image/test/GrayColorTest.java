import org.junit.*;
import static org.junit.Assert.*;

import image.color.GrayColor;

public class GrayColorTest {

  @Test
  public void testGrayColorCreation(){
    GrayColor gc = new GrayColor(112);
    assertNotNull(gc);
    assertSame(gc.getGrayLevel(),112);
  }


  @Test
    public void testGrayColorEquals(){
      GrayColor a = new GrayColor(246);
      GrayColor b = new GrayColor(200);
      assertFalse(a.equals(b));
      assertTrue(a.equals(a));
}
    // ---Pour permettre l'exécution des test----------------------
    public static junit.framework.Test suite() {
        return new junit.framework.JUnit4TestAdapter(GrayColorTest.class);
    }

}
