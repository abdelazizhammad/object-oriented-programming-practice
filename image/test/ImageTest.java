import org.junit.*;
import static org.junit.Assert.*;

import image.*;
import image.color.GrayColor;

public class ImageTest{

  @Test
  public void testImageCreation(){
    Image i = new Image(10,30);
    assertNotNull(i);
    assertSame(i.getWidth(), 10);
    assertSame(i.getHeight(), 30);
  }

    @Test
    public void testImageSetPixel(){
      try{
        Image i = new Image(10,5);
        Pixel new_p = new Pixel(GrayColor.BLACK);
        Pixel p;
        p = i.getPixel(0,0);
        assertSame(p.getColor(), GrayColor.WHITE);
        i.setPixel(0,0,new_p);
        p=i.getPixel(0,0);
        assertSame(p.getColor(), GrayColor.BLACK);
        assertSame(p, new_p);

        p = i.getPixel(9,4);
        assertSame(p.getColor(), GrayColor.WHITE);
        i.setPixel(9,4,new_p);
        p=i.getPixel(9,4);
        assertSame(p.getColor(), GrayColor.BLACK);
        assertSame(p, new_p);
      }
      catch(UnknownPixelException e){
        assertFalse(true);
      }
    }

    @Test
    public void testImageChangePixelColor(){
      try{
        Image i = new Image(10,5);
        Pixel p;
        p=i.getPixel(0,0);
        assertSame(p.getColor(), GrayColor.WHITE);
        i.changeColorPixel(0,0, GrayColor.BLACK);
        Pixel p1=i.getPixel(0,0);
        assertSame(p,p1);
        assertSame(p.getColor(), GrayColor.BLACK);
        assertSame(p1.getColor(), GrayColor.BLACK);

      }
      catch(UnknownPixelException e){
        assertFalse(true);
      }
    }

    @Test
    public void testImageEdge(){
      try{
        Image i = new Image(5,5);
        GrayColor g100=new GrayColor(100);
        GrayColor g90=new GrayColor(90);
        GrayColor g79=new GrayColor(79);
        i.changeColorPixel(0,0, g100);
        i.changeColorPixel(0,1, g90);
        i.changeColorPixel(1,0, g90);
        i.changeColorPixel(2,0, g90);
        i.changeColorPixel(1,1, g79);
        Image i2=i.edge(10);
        assertEquals(i2.getWidth(), i.getWidth());
        assertEquals(i2.getHeight(), i.getHeight());
        for(int x=0; x<5; x++){
          for(int y=0; y<5; y++){
            if((y==0 &&(x==1 || x==2)) || (y==1 && (x==1 || x==0))){
              assertEquals(i2.getPixel(x,y).getColor(), GrayColor.BLACK);
            }
            else{
              assertEquals(i2.getPixel(x,y).getColor(), GrayColor.WHITE);
            }
          }
        }
      }
      catch(UnknownPixelException e){
        assertFalse(true);
      }
    }


    @Test
    public void testImageDecreaseGrayLevels(){
      try{
        Image i =new Image(1,256);
        for(int y=0; y<256; y++){
          i.changeColorPixel(0,y,new GrayColor(y));
        }
        Image new_i = i.decreaseNbGrayLevels(2);
        assertEquals(new_i.getWidth(), i.getWidth());
        assertEquals(new_i.getHeight(), i.getHeight());
        GrayColor g128= new GrayColor(128);
        for(int y=0; y<128; y++){
          assertEquals(new_i.getPixel(0,y).getColor(),GrayColor.BLACK);
        }
        for(int y=128; y<256; y++){
          assertEquals(new_i.getPixel(0,y).getColor(),g128);
        }
      }
      catch(UnknownPixelException e){
        assertFalse(true);
      }
    }


  public static junit.framework.Test suite(){
    return new junit.framework.JUnit4TestAdapter(ImageTest.class);
  }

}
