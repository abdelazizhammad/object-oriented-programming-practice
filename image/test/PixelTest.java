import org.junit.*;
import static org.junit.Assert.*;

import image.Pixel;
import image.color.GrayColor;

public class PixelTest {

  @Test
  public void testPixelCreation(){
    GrayColor gc = new GrayColor(112);
    Pixel p = new Pixel(gc);
    assertNotNull(p);
    assertSame(p.getColor(),gc);
  }

  @Test
  public void testPixelSetColor(){
    GrayColor gc1 = new GrayColor(112);
    GrayColor gc2 = new GrayColor(112);
    Pixel p = new Pixel(gc1);
    assertSame(p.getColor(),gc1);
    assertFalse(p.getColor()==gc2);
    p.setColor(gc2);
    assertSame(p.getColor(),gc2);
    assertFalse(p.getColor()==gc1);
  }

  
@Test
    public void testPixelEquals(){
      GrayColor a = new GrayColor(255);
      GrayColor b = new GrayColor(200);
      Pixel p = new Pixel(a);
      Pixel h = new Pixel(b);
      assertFalse(p.equals(h));
      assertTrue(p.equals(p));
}
@Test
    public void testPixelDifference(){
      GrayColor a = new GrayColor(255);
      GrayColor b = new GrayColor(200);
      Pixel p = new Pixel(a);
      Pixel h = new Pixel(b);
      assertEquals(55,p.colorLevelDifference(h));
      assertEquals(0,p.colorLevelDifference(p));
}
    // ---Pour permettre l'exécution des test----------------------
    public static junit.framework.Test suite() {
        return new junit.framework.JUnit4TestAdapter(PixelTest.class);
    }

}
