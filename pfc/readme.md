# TP7 : PFC

**Abdelaziz & Khaled**




### Le TP

L'objectif de ce TP est de r´ealiser un programme qui permette de jouer au jeu Pierre-Feuille-Ciseaux. 
### Documentation
Pour générer la documentation il faut se placer dans le répertoire "pfc/src/rps" et exécuter la commande `javadoc nomDeClass -d ../docs` 

Pour voir la documentation générée il suffit de consulter le fichier "index.html" dans le répertoire docs résultant de la commande précédente.



### Compilation

Pour compiler les différentes classes on réalise la commande :
javac NomClass.java;
	
        `javac Game.java`
        `javac DrowException.java`
        `javac player.java`
        `javac Input.java`
        `javac GameTest.java`
        `javac ShapeTest.java`
        `javac PlayerTest.java`

	
On retrouve les fichiers compilés dans le répertoire *classes/*


### Exécution

Pour exécuter, on utilise les commandes suivantes: 

`java Game` ;
`java Player` ;
`java Input` ;
`java DrowException ` ;
`java ShapeTest` ;
`java PlayerTest`;
`java GameTest`;

### Archive

pour créer une archive on exécute la commande suivante:
`· · · /RPS/classes> jar cvf ../rps.jar rps`

