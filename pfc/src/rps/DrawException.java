/**
 * This class creates the DrawException.
 * @author Khalid & Abdelaziz
 * @version 2020.11
 */

 package rps;

 public class DrawException extends Exception{
   public DrawException(String msg){
     super(msg);
   }
 }
