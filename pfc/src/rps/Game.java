/**
 * This class models the Rock-Paper-Scissors game.
 * @author Khalid & Abdelaziz
 * @version 2020.11
 */

package rps;
import rps.strategy.*;
import rps.shape.Shape;
import java.util.*;

public class Game{
  private Player p1;
  private Player p2;
  private Strategy s1;
  private Strategy s2;
  private Map<Player, Strategy> players;

  /**
   * Constructs a Rock-Paper-Scissor game with two players.
   * @param p1 the first player
   * @param p2 the second player
   */
  public Game(Player p1, Player p2, Strategy s1, Strategy s2){
    this.p1 = p1;
    this.p2 = p2;
    this.s1 = s1;
    this.s2 = s2;
    this.players = new HashMap<Player, Strategy>();
    this.players.put(this.p1, this.s1);
    this.players.put(this.p2, this.s2);
  }

  /**
   * Gives the set of the players.
   * @return both players
   */
  public Set<Player> getPlayers(){
    return this.players.keySet();
  }

  /**
   * Tells about the winner of the game.
   * @param nbRounds the rounds' number of the game
   * @return the winner if there's one, draw otherwise
   * @exception DrawException when the result is a draw
   */
  public Player play(int nbRounds) throws DrawException{
    int scoreP1 = 0;
    int scoreP2 = 0;
    for(int i=0; i<nbRounds; i++){
      Shape sh1 = this.s1.chooseShape();
      Shape sh2 = this.s2.chooseShape();
      if(sh1.Comp(sh2) == -1){
        scoreP2 += 2;
        System.out.println(this.p1.getName() + " has played " + sh1.toString() + ". " + this.p2.getName() + " has played " + sh2.toString() + ".");
        System.out.println(this.p2.getName() + " wins and scores 2 points.");
        System.out.println("The score is now : " + this.p1.getName() + " = " + scoreP1 + " points - " + this.p2.getName() + " = " + scoreP2 + " points");
      }
      if(sh1.Comp(sh2) == 0){
        scoreP1 += 1;
        scoreP2 += 1;
        System.out.println(this.p1.getName() + " has played " + sh1.toString() + ". " + this.p2.getName() + " has played " + sh2.toString() + ".");
        System.out.println("This round ended up with a draw and both players got 1 point.");
        System.out.println("The score is now : " + this.p1.getName() + " = " + scoreP1 + " points - " + this.p2.getName() + " = " + scoreP2 + " points");
      }
      if(sh1.Comp(sh2) == 1){
        scoreP1 += 2;
        System.out.println(this.p1.getName() + " has played " + sh1.toString() + ". " + this.p2.getName() + " has played " + sh2.toString() + ".");
        System.out.println(this.p1.getName() + " wins and scores 2 points.");
        System.out.println("The score is now : " + this.p1.getName() + " = " + scoreP1 + " points - " + this.p2.getName() + " = " + scoreP2 + " points");
      }
    }
    if(scoreP1 > scoreP2){
      System.out.println(this.p1.getName() + " has won!");
      return this.p1;
    }
    if(scoreP1 < scoreP2){
      System.out.println(this.p2.getName() + " has won!");
      return this.p2;
    }
    else{
      throw new DrawException("Draw!");
    }
  }
}
