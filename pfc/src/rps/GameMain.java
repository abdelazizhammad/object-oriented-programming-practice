/**
 * This is a main class simulating a game between a human player and the cpu.
 * @author Khalid & Abdelaziz
 * @version 2020.11
 */

 package rps;
 import rps.strategy.*;

 public class GameMain{
   public static void main(String[] args){
     Player p1 = new Player("Hassan");
     Player p2 = new Player("CPU");
     Strategy s1 = new HumanStrat();
     Strategy s2 = new RandomStrat();
     Game shifumi = new Game(p1, p2, s1, s2);
     try{
       shifumi.play(Integer.parseInt(args[0]));
     }
     catch(DrawException d){
       System.out.println("Draw!");
     }
   }
 }
