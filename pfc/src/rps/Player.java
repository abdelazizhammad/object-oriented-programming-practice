/**
 * This class creates players for the Rock-Paper-Scissor game.
 * @author Khalid & Abdelaziz
 * @version 2020.11
 */

 package rps;

 public class Player{
   private String name;

   /**
    * Constructs a player according to his name.
    * @param namae the player's name
    */
   public Player(String namae){
     this.name = namae;
   }

   /**
    * Gives the player's name.
    * @return the player's name
    */
   public String getName(){
     return this.name;
   }

   /**
    * Modifies the player's name.
    * @param nom the new name suggested
    */
   public void setName(String nom){
     this.name = nom;
   }

   /**
    * Tells whether two players are the same or not.
    * @param o an object to compare with
    * @return <code>true</code> if both players are the same, <code>false</code> otherwise
    */
   public boolean equals(Object o){
     if(o instanceof Player){
       Player other = (Player) o;
       return this.name == other.name;
     }
     else{
       return false;
     }
   }
 }
