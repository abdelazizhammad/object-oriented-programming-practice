/**
 * This class enumerates players the available shapes in the shifumi game and their order of superiority.
 * @author MatHassan
 * @version 2020.11.18
 */

 package rps.shape;
 import java.util.Random;

 public enum Shape{
   ROCK, PAPER, SCISSORS;

   /**
    * Indicates the superiority of this shape in comparison with another shape.
    * @param s a shape to compare the first one with
    * @return -1 if this shape is beaten by s, 0 if this shape is s, 1 otherwise
    */
   public int Comp(Shape s){
     if(this == s){
       return  0;
     }
     if(this == ROCK){
       if(s == PAPER){
         return -1;
       }
       else{
         return 1;
       }
     }
     if(this == PAPER){
       if(s == SCISSORS){
         return -1;
       }
       else{
         return 1;
       }
     }
     if(this == SCISSORS){
       if(s == ROCK){
         return -1;
       }
       else{
         return 1;
       }
     }
     return 0;
   }

   private static Random alea = new Random();

   /**
    * Returns a shape at random among the three available.
    * @return a shape at random
    */
   public static Shape random(){
     return Shape.values()[alea.nextInt(Shape.values().length)];
   }

   /**
    * Turns each Shape object to the corresponding string.
    * @return the corresponding string
    */
   public String toString(){
     if(this == ROCK){
       return "ROCK";
     }
     if(this == PAPER){
       return "PAPER";
     }
     return "SCISSORS";
   }
 }
