/**
 * These classes model several strategies by means of an interface.
 * @author Khalid & Abdelaziz
 * @version 2020.11
 */

 package rps.strategy;
 import rps.shape.Shape;
 import java.util.Scanner;

 public class HumanStrat implements Strategy{
   private static Scanner scanner = new Scanner(System.in);
   public Shape chooseShape(){
     System.out.print(" What do you play ? ");
     int strat = this.scanner.nextInt();
     if(strat == 1){
       return Shape.ROCK;
     }
     if(strat == 2){
       return Shape.PAPER;
     }
     if(strat == 3){
       return Shape.SCISSORS;
     }
     return Shape.random();
   }
 }
