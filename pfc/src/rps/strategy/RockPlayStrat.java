/**
 * These classes model several strategies by means of an interface.
 * @author Khalid & Abdelaziz
 * @version 2020.11.18
 */

 package rps.strategy;
 import rps.shape.Shape;

 public class RockPlayStrat implements Strategy{
   public Shape chooseShape(){
     return Shape.ROCK;
   }
 }
