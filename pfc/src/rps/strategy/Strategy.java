/**
 * These classes model several strategies by means of an interface.
 * @author Khalid & Abdelaziz
 * @version 2020.11
 */

 package rps.strategy;
 import rps.shape.Shape;

 public interface Strategy{
   public Shape chooseShape();
 }
