/**
* @author Khalid & Abdelaziz
* @version 2020.11 
 */

 import static org.junit.Assert.*;
 import org.junit.Test;

 import rps.*;
 import rps.strategy.*;
 import java.util.*;

 public class GameTest{

   @Test
   public void dataAreCorrectAtCreation(){
     Player p1 = new Player("Matthew");
     Player p2 = new Player("CPU");
     Strategy s1 = new HumanStrat();
     Strategy s2 = new RandomStrat();
     Game goh = new Game(p1, p2, s1, s2);
     Set<Player> p = goh.getPlayers();
     Map<Player, Strategy> play = new HashMap<Player, Strategy>();
     play.put(p1, s1);
     play.put(p2, s2);
     Set<Player> q = play.keySet();
     assertEquals(p, q);
   }

   @Test(expected = DrawException.class)
   public void playThrowsDrawExceptionIfNoPlayerWon() throws DrawException{
     Player p1 = new Player("Matthew");
     Player p2 = new Player("CPU");
     Strategy s1 = new RockPlayStrat();
     Strategy s2 = new PaperPlayStrat();
     Strategy s3 = new RockPlayStrat();
     Game goh = new Game(p1, p2, s1, s2);
     Game tog = new Game(p1, p2, s1, s3);
     try{
       goh.play(1);
     }
     catch(DrawException e){
       fail();
     }
     tog.play(1);
   }

   public static junit.framework.Test suite() {
       return new junit.framework.JUnit4TestAdapter(GameTest.class);
   }
 }
