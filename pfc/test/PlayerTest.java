/**
 * This class tests the Player class.
 * @author Khalid & Abdelaziz
 * @version 2020.11
 */

 import static org.junit.Assert.*;
 import org.junit.Test;

 import rps.Player;

 public class PlayerTest{

   @Test
   public void dataAreCorrectAtCreation(){
     Player p = new Player("Hassan");
     assertEquals("Hassan", p.getName());
   }

   @Test
   public void gotNameIsCorrect(){
     Player p = new Player("Matthew");
     String namae = p.getName();
     assertEquals("Matthew", namae);
   }

   @Test
   public void nameIsCorrectlySet(){
     Player p = new Player("Hassan");
     assertEquals("Hassan", p.getName());
     p.setName("Matthew");
     assertEquals("Matthew", p.getName());
   }

   @Test
   public void playersAreCorrectlyCompared(){
     Player p1 = new Player("Hassan");
     Player p2 = new Player("Matthew");
     Player p3 = new Player("Hassan");
     assertFalse(p1.equals(p2));
     assertTrue(p1.equals(p3));
   }

   public static junit.framework.Test suite() {
       return new junit.framework.JUnit4TestAdapter(PlayerTest.class);
   }
 }
