/**
 * This class tests the Shape class.
 * @author Khalid & Abdelaziz
 * @version 2020.11
 */

 import static org.junit.Assert.*;
 import org.junit.Test;

 import rps.shape.Shape;

 public class ShapeTest{

   @Test
   public void dataAreCorrectAtCreation(){
     Shape sh1 = Shape.ROCK;
     Shape sh2 = Shape.PAPER;
     Shape sh3 = Shape.SCISSORS;
     assertTrue(sh1 instanceof Shape);
     assertTrue(sh2 instanceof Shape);
     assertTrue(sh3 instanceof Shape);
   }

   @Test
   public void shapesAreCorrectlyCompared(){
     Shape sh1 = Shape.ROCK;
     Shape sh2 = Shape.PAPER;
     Shape sh3 = Shape.SCISSORS;
     assertEquals(sh1.Comp(sh2), -1);
     assertEquals(sh1.Comp(sh3), 1);
     assertEquals(sh2.Comp(sh3), -1);
     assertEquals(sh2.Comp(sh1), 1);
     assertEquals(sh3.Comp(sh1), -1);
     assertEquals(sh3.Comp(sh2), 1);
     assertEquals(sh1.Comp(sh1), 0);
     assertEquals(sh2.Comp(sh2), 0);
     assertEquals(sh3.Comp(sh3), 0);
   }

   @Test
   public void shapeIsTransformedToString(){
     Shape sh1 = Shape.ROCK;
     Shape sh2 = Shape.PAPER;
     Shape sh3 = Shape.SCISSORS;
     assertEquals(sh1.toString(), "ROCK");
     assertEquals(sh2.toString(), "PAPER");
     assertEquals(sh3.toString(), "SCISSORS");
   }

   public static junit.framework.Test suite() {
       return new junit.framework.JUnit4TestAdapter(ShapeTest.class);
   }
 }
