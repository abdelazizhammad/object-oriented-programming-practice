
/**
 * Write a description of class Goods here.
 *
 * @author (your name)
 * @version (a version number or a date)
 */
public class Goods
{
    // instance variables 
    private double value;
    private String name;


    /**
     * Constructor for objects of class Goods
     */
    public Goods(String nm)
    {
        // initialise instance name
        this.name = nm ;
        this.value = 0; 
    }
    
    /**
     * An example of a method - replace this comment with your own
     *
     * @param  y  a sample parameter for a method
     * @return    the sum of x and y
     */
    public double getValue() {
     return this.value ;
    }
    
    public void setValue(double v){
      this.value = v ;
    }
    
    public String getName(){
      return this.name ;
    }
    
    public String toString(){
    return "the goods " +  name + " costs " + value;
    }
    
    public double totalCost() {
     return this.value * 1.20 ;
    }
    
    
    
    

}
