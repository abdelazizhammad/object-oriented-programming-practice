
/**
 * Write a description of class LightString here.
 *
 * @author HAMMAD AHMED Abdelaziz
 * @version 29/09/2020
 */
public class LightString
{
    /**The bulbs in the light string */
    private Lightbulb[] Lstring;
    /**The state of the light string */
    private boolean on;
    /**The total power consumed*/
    private int Tpower;
    
    /**
     * Creates a LightString containing num Lightbulbs. All lightbulbs created are "White" and have a 1W power.
     * 
     * @param num the number of lightbulbs created
     * 
     */
    public LightString(int num){
        this.Lstring = new Lightbulb[num];
        for(int i=0; i<num; i++){
            Lstring[i] = new Lightbulb(1, "White");
        }
        this.on=false;
        this.Tpower=num;
    }
    
    /**
     * Returns the i-th Lightbulb in the LightString. Returns null if i isn't a valid index.
     * 
     * @param i the number of the lightbulb to be changed (first has number 1)
     * 
     * @return the i-th Lightbulb in the LightString.
     */
    public Lightbulb getBulb(int i){
        return ((0<i) && (i<=this.Lstring.length)? this.Lstring[i-1] : null);
    }
    
    
        
    /**
     * Returns the total power consumed by the light string
     * 
     * @return the total power consumed by the light string
     */
    public int getConsumedPower(){
        return this.Tpower;
    }
    
    
    /**
     * Adds n W to the total power consumed by the light string
     * 
     * @param n the power to add (can be negative)
     */
    private void addPower(int n){
        this.Tpower+=n;
    }
    

    /** replace the n-th lightbulb of the light string by the given lightbulb.
     * Nothing happens if i is not a valid index.
     * The given lightbulb automatically changes state to this of the bulbs of the lightstring.
     * 
     * @param i the number of the lightbulb to be changed (first has number 1)
     * 
     * @param theBulb the new lightbulb
    */
    public void changeLightbulb(int i, Lightbulb theBulb){
        if(0<i && i<=this.Lstring.length){
            this.addPower(-(this.Lstring[i-1].getPower()));
            this.addPower(theBulb.getPower());
            if(this.isOn()){
                theBulb.on();
            }
            else{
                theBulb.off();
            }
            this.Lstring[i-1] = theBulb;
        }
    }
    
    /**
     * Returns true if the light string is on, false otherwise
     * 
     * @return true if the light string is on, false otherwise 
     */
    public boolean isOn(){
        return this.on;
    }
    
    /**
     * Changes the state of all the light bulbs in the light string
     */
    public void pressSwitch(){
        if(this.isOn()){
            for(int i=0; i<this.Lstring.length; i++){
                this.Lstring[i].off();  //Je n'ai pas utilisé getBulb pour ne pas effectuer deux comparaisons par ampoule
            }
        }
        else{  //J'ai fait de cette manière pour éviter d'avoir une comparaison dans la boucle qui se répète inutilement
            for(int i=0; i<this.Lstring.length; i++){
                this.Lstring[i].on();
            }
        }
        this.on=!this.isOn();
    }
    

        
    public String toString(){
        return "This light string contains "+this.Lstring.length+" light bulbs.\nIt is now "+
        (this.isOn()?"on.\nThe total power consumed is ":"off.\nIf it were open, the total power consumed would be ")+
        this.getConsumedPower()+" W.";
    }
    
    
}    
        
        