
/**
 * Write a description of class Switch here.
 *
 * @author HAMMAD AHMED Abdelaziz
 * @version 29/09/2020
 */
public class Lightbulb
{
    /** The lightbulb's power */    
    private double power;
    /** The lightbulb's color */
    private String color;
    /** The lightbulb's state (on or off)   */   
    private boolean on ;
    
    /**
     * Creates a lightbulb
     * 
     * @param power the lightbulb's power in watts
     * 
     * @param color the lightbulb's color
     * 
     * @param on if true the lightbulb would be on, if false it would be off.
     */
    public Lightbulb(double pow, String col, boolean on)
    {
        // initialise instance variables
        this.power= pow ;
        this.color =  col ;
        this.on = on ;
    }
    
    /**
     * Creates a lightbulb
     * 
     * @param power the lightbulb's power in watts
     * 
     * @param color the lightbulb's color
     */
    public Lightbulb(double pow, String col)
    {
        // initialise instance variables
        this.power = pow ;
        this.color =  col ;
        this.on = false ;
    }

    /**
     * Returns the lightbulb's power
     * 
     * @return the lightbulb's power
     */
    public double getPower()
    {
       return this.power ;
    }
    
    /**
     * Returns the lightbulb's color
     * 
     * @return the lightbulb's color
     */
    public String getColor() {
     return this.color;
    }
    
      /**
     * Returns true if the lightbulb is on, false otherwise
     * 
     * @return true if the lightbulb is on, false otherwise
     */
    public boolean isOn(){
        
     return this.on ;
    }
    
    /**
     * Turns the lightbulb on
     */
    public void on(){
        this.on = true ;
    }

    /**
     * Turns the lightbulb off
     */
    public void off(){
       this.on = false ; 
    }
    
    /**
     * Returns a string representing the lightbulb
     * 
     * @return a string representing the lightbulb
     */
    public String toString(){
      return " The Light bulb is on ? " +  this.isOn() + "\npower = " + this.getPower() + "w\n" + "color = " + this.getColor() ;
     
    }
    
} 
