
/**
 * Write a description of class Stock here.
 *
 * @author (your name)
 * @version (a version number or a date)
 */
public class Stock
{
    private int quantity;
   public Stock () {
    this.quantity = 0 ;
   }
   
   public Stock (int init) {
    this.quantity = init ;
   } 
    
    public int getQuantity () {
     return this.quantity;
   }
   
   
   public void add (int n){
      quantity =  this.quantity + n ;
    }
    
   public int remove (int rem){
      
       if (this.quantity - rem >= 0){
           this.quantity = this.quantity - rem;
           return rem;
        }
    
       else {
           int temp = this.quantity ;
           this.quantity = 0 ;
           return temp;
       }
     }
   
   public String toString (){
    return "the stock’s quantity is" + this.quantity ;
    }




}
