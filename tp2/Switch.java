
/**
 * Write a description of class Switch here.
 *
 * @author HAMMAD AHMED Abdelaziz
 * @version 29/09/2020
 */
public class Switch
{
    /** The lightbulb linked to the switch*/ 
    private Lightbulb lightbulb;
        /** The switch's state  */
    private boolean on ;

    /**
     * Creates a switch and links it to the lightbulb passed as parameter
     * 
     * @param lightbulb is a lightbulb
     */
    public Switch(Lightbulb lightbulb)
    {
        // initialise instance variables
        this.lightbulb = lightbulb;
        this.on = lightbulb.isOn();
    }
    
    /**
     *  Returns true if the linked lightbulb is on, false otherwise 
     *  
     *  @return  true if the linked lightbulb is on, false otherwise
     */
    public boolean getState(){
      return this.on;
    }
    
    /**
     * changes the switch's state
     */
    
    public void setState(){
      this.on =! this.getState();
    }
    
    /**
     * Returns the lightbulb linked to this switch
     * 
     * @return the lightbulb linked to this switch
     */
    public Lightbulb getLightbulb(){
        return this.lightbulb;
    }
    
    /**
     * Changes the lightbulb's state (on to off and vice versa)
     */
    public void pressSwitch()
    {
        // 
        if ( this.lightbulb.isOn() ) {
           
            this.getLightbulb().off();
        }
        else{
            this.getLightbulb().on();
        }
        this.setState();
  
  
    
    }


}
