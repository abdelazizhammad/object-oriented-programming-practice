/**
* une classe Complexe qui permet de représenter des des nombres Complexes.
* @author hammadahmed
*/

import java.util.Scanner;
public class Complexe{
private double reel;
private double imaginaire;

public Complexe(double r,double im){
    this.reel = r;
    this.imaginaire = im;
}

public double realPart(){
    return this.reel;
}

public double imaginaryPart(){
    return this.imaginaire;
}

public boolean isReal(){
    return this.imaginaire==0;
}

public Complexe conjugate (){
        return new Complexe(this.reel,-(this.imaginaire));
}

public double module(){
    double module = Math.sqrt(Math.pow(this.reel,2)+Math.pow(this.imaginaire,2));
    return module;
}

public Complexe add(Complexe c){
    double re=this.reel+c.reel;
    double im=this.imaginaire+c.imaginaire;
    return new Complexe(re,im);
}

public Complexe multi(Complexe c){
    double re=this.reel*c.reel;
    double im=this.imaginaire*c.imaginaire;
    return new Complexe(re,im);
}

public boolean equals(Complexe c){
    return(this.reel==c.reel)&&(this.imaginaire==c.imaginaire);
}
public String toString(){
    return this.reel+" + "+this.imaginaire+"i";
}
public static void main (String[] args){
 Scanner sc=new Scanner(System.in);
 double r1,r2,im2,im1;
 System.out.println("entrer la partie reel et la partie imaginaire ");
 r1=sc.nextDouble();
 im1=sc.nextDouble();
 System.out.println("entrer la partie reel et la partie imaginaire ");
 r2=sc.nextDouble();
 im2=sc.nextDouble();
 Complexe c1 = new Complexe(r1,im1);
 Complexe c2 = new Complexe(r2,im2);
 Complexe conjugC1 =c1.conjugate();
 Complexe conjugC2=c2.conjugate();
 Complexe somme=c1.add(c2);
 Complexe produit=c1.multi(c2);
 System.out.println("le conjugue de "+c1.toString()+" est : "+conjugC1.toString());
 System.out.println("le conjugue de "+c2.toString()+" est : "+conjugC2.toString());
 System.out.println("la somme de  "+c1.toString()+" et  "+c2.toString()+" est : "+somme.toString());
 System.out.println("le produit de  "+c1.toString()+" et  "+c2.toString()+" est : "+produit.toString());
}
}
