/**
 * une classe Rectangle qui permet de repr´esenter des rectangles
 * @author hammadahmed
 */
public class Rectangle {

    /**
    * Un objet Rectangle est
      caract´eris´e par les valeurs de sa «longueur» et de sa « largeur », de type double. Ces donn´ees sont fournies lors de la
      construction d’un objet Rectangle.
    *
    */

    private double longeur;
    private double largeur;



    public Rectangle(double lar, double lon){
     this.largeur = lar;
     this.longeur = lon;
     }





    public double getLargeur(){
     return this.largeur;
     }

    public double getLongeur(){
     return this.longeur;
     }

    public double aire(){
     return this.largeur*this.longeur;

    }

    public double perimetre(){
     return 2*this.largeur + 2*this.longeur;

    }


    public boolean isSquare(){
    return this.largeur == this.longeur;
    }

    public boolean equals(Object o){
     if (o instanceof Rectangle){
       Rectangle other = (Rectangle) o;
       return this.largeur == other.largeur && this.longeur == other.longeur;
     }
     else {return false;}

    }



    public String toString(){

       return "Le Rectangle a un largeur de " + this.largeur + " et un loneur de " +  this.longeur;

    }



     /**
     * @param args the command line arguments
     */

     public static void main(String[] args) {
        Rectangle r1 = new Rectangle(4, 5);
        Rectangle r2 = new Rectangle(5, 5);
        System.out.println("Aire = " + r1.aire());
        System.out.println("perimetre = " + r1.perimetre());

        System.out.println("r1 carré? " + (r1.getLargeur()==r1.getLongeur()));

        System.out.println("r2 carré? " + (r2.getLargeur() == r2.getLongeur()));

        System.out.println("r1 = r2 ? " + r1.equals(r2));

        ;


    }







}
