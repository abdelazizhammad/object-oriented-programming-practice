Abdelaziz HAMMAD AHMED
=====================================

Objectifs
---------
Ce TP avait comme but de de coder ses premières classes, d’apprendre à compiler et d’´ecrire des m´ethodes main et les ex´ecuter




Classes
-------
Le répertoire contient les classes:

* Rectangle.java : Compiler grâce à la commande `javac Rectangle.java`

* Complexe.java : Compiler grâce à la commande `javac Complexe.java`



Exécution
---------


Pour l'exécuter, utiliser la commande `java Rectangle` et `java Complexe.java` .

