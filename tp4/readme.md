Abdelaziz HAMMAD AHMED
=====================================

Objectifs
---------
Ce TP avait comme but de manipuler javadoc, junit et de s'entrainer à coder les méthodes des classes de manière à ce qu'elles soient conformes aux tests.



La Documentation
----------------
Pour générer la documentation il faut se placer dans le répertoire qui contient le fichier et exécuter la commande `javadoc fichier.java -d docs`

Les autres classes n'ont pas de documentation.

Pour voir la documentation générée, il suffit de consulter le fichier "index.html" dans le répertoire docs résultant de la commande précédente.


Classes
-------
Le répertoire contient les classes:

* Bike.java : Compiler grâce à la commande `javac Bike.java`

* BikeModel.java : Compiler grâce à la commande `javac BikeModel.java`

* BikeStation.java : Compiler grâce à la commande `javac BikeStation.java`



Tests
-----



* Compiler grâce à la commande `javac -classpath test-1.7.jar test/BikeTest.java`
* Compiler grâce à la commande `javac -classpath test4poo.jar test/BikeStationTest.java`
* Lancer grâce à la commande `java -jar test-1.7.jar BikeTest`
* Lancer grâce à la commande `java -jar test-1.7.jar BikeStationTest`


Exécution
---------


Pour l'exécuter, utiliser la commande `java Bike <___>`
et selon les fonctions vous ajouterez un parametre en plus 

