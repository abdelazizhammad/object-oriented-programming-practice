
/**
 * A class to design bike station that can receive rented bikes.
 * BikeStation has a name and a difined noumber of slots for bikes.
 *
 * @author Abdelaziz HAMMAD AHMED
 *
 */

public class BikeStation {

    /** array of slots for bikes in the station */
    private Bike[] bikes;
    /** name of the station */
    private String name;
    /** the capacity of the station */
    private int capacity ;

    /** create a BikeStation with given name and capacity
  	 * @param name this BikeStation's name
  	 * @param capacity this BikeStation's capacity
  	 */
	public BikeStation(String name, int capacity) {
    this.name = name;
    this.bikes = new Bike[capacity] ;
    this.capacity = capacity ;

	}

  /** return this BikeStation's name
   * @return the name of this BikeStation
   */
	public String getName() {

		return this.name;
	}

  /** return this BikeStation's capacity
   * @return the capacity of this BikeStation
   */
	public int getCapacity() {

		return this.capacity;
	}

  /** return the actual number of bikes in this BikeStation
   * @return the number of bikes this BikeStation
   */
	public int getNumberOfBikes() {
    int n = 0 ;
    for (int i = 0; i < this.bikes.length; i++) {
      if (this.bikes[i] != null)
      n++;
    }
    return n;
	}


  /** return the position of the first free slot in this BikeStation
   * returns -1 if there is no free slot
   * @return the first free slot this BikeStation or -1 if there is no one
   */
	public int firstFreeSlot() {
    int x = -1 ;

    for (int i = 0 ; i < this.bikes.length ; i ++){
      if (this.bikes[i] == null){
        x = i ;
        return x;
      }

    }
    return x ;

	}

  /** return false if there is no free slot in this BikeStation
   *  drop the bike is the firstFreeSlot and retruns true
   * @return false if there is no free slot in this BikeStation or true if there is a free slot in this BikeStation
   *
   */
	public boolean dropBike(Bike bike) {
	    if (this.firstFreeSlot() == -1){
        return false;
      }
      else {
        this.bikes[this.firstFreeSlot()] = bike ;
        return true;

      }
	}



  /** return a bike took from the position i
   *  return null if there is no bike in position i
   * @return a bike from this position i or it returns null
   */
	public Bike takeBike(int i) {
    if (i < 0 || i > this.bikes.length || this.bikes[i] == null){
      return null ;
    }
    else{
      Bike temp = this.bikes[i] ;
      this.bikes[i] = null ;
      return temp ;
    }


	}


}
