/**
 * A class which describes the workings of a bike station.
 * @author HAMMAD AHMED Abdelaziz
 *
 */

import java.util.Scanner;

class BikeStationMain {
	public static void main(String[] args){
		Scanner empl = new Scanner(System.in);
		int n = empl.nextInt();
    BikeStation station1 = new BikeStation("Timoleon", 10);
		System.out.println(station1.dropBike(new Bike("b001", BikeModel.CLASSICAL)));
		System.out.println(station1.dropBike(new Bike("b002", BikeModel.ELECTRIC)));
		if (n>=2){
			System.out.println("il n'y a pas de vélo");
		}
		else{
			System.out.println(station1.takeBike(n).getId());
		}
	}
}
