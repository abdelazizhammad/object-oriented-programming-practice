Abdelaziz HAMMAD AHMED
======================

TP5
===

Objectifs
---------
Ce TP avait comme but de manipuler les paquetages,de réaliser la compilation des classes,la génération de la documentation,d'executer les fichiers compilés par certains chemins en utilisant certaines fonctions,de générer des archives et l'execution des archives.


La Documentation
----------------
Pour générer la documentation il faut se placer dans le répertoire "tp5/src" et exécuter la commande `javadoc date -d ../docs` 

Pour voir la documentation générée il suffit de consulter le fichier "index.html" dans le répertoire docs résultant de la commande précédente.


Classes
-------
Le répertoire "classes" contient un autre répertoire date contenant les fichiers des classes compilé :

* Date.java : Compiler grâce à la commande `javac date/Date.java -d ../classes`  depuis le dossier src 

* Month.java : Compiler grâce à la commande `javac date/Month.java -d ../classes`  depuis le dossier src 
* la commande `javac date/*.java -d ../classes` compile tous les fichier .java

Tests
-----

* DateTest.java :depuis le répertoire racine TP5
* Compiler grâce à la commande `javac -classpath test-1.7.jar test/DateTest.java`
* Lancer grâce à la commande `java -jar test-1.7.jar DateTest`
* MonthTest.java :
* Compiler grâce à la commande `javac -classpath test-1.7.jar test/MonthTest.java`

    * Lancer grâce à la commande `java -jar test-1.7.jar MonthTest`

Archive
-------


* Création : depuis classes `jar cvf ../date.jar date`
* faire des jar exécutables : depuis classes `jar cvfm ../date.jar ../manifest-date date`
* Une fois l'archive créée, depuis tp5 `java -jar factory.jar` 


Exécution
---------
* Depuis calsses `java date.DateMain`
* Depuis tp5 `java -classpath classes date.DateMain`
* Date.java `java -jar test-1.7.jar DateTest`
* Month.java `java -jar test-1.7.jar MonthTest`
* Exécution d'archive depuis tp5 `java -classpath date.jar date.DateMain`
ou `java -jar factory.jar`








