package date;
/**
 * representation of a date
 *
 * @author Abdelaziz HAMMAD AHMED
 * @version 07/10/2020
 */
public class Date {

	private int day;
	private Month month;
	private int year;

	/**
	 * build a date with given day, month and year (we do not check if date is
	 * valid, by example if day>0 )
	 *
	 * @param day   the day for this date
	 * @param month the month for this date
	 * @param year  the year for this date
	 */
	public Date(int day, Month month, int year) {
		// no validity check
		this.day = day;
		this.month = month;
		this.year = year;
	}

	/**
	 * return day of this
	 *
	 * @return day of this
	 */
	public int getDay() {
		return this.day;
	}

	/**
	 * return month of this
	 *
	 * @return month of this
	 */
	public Month getMonth() {
		return this.month;
	}

	/**
	 * return year of this
	 * @return year of this public int getYear() { return this.year; }
	 */
	public int getYear() {
	 return this.year;
  }

	/**
  * returns <code>true</code> <code>if</code> two dates are the same
  * @param o an object
  * @return <code>true</code> <code>if</code> two dates are the same
  */
  public boolean equals(Object o)
  {
    if (o instanceof Date)
    {
      Date other = (Date)o;
      return this.day == other.day && this.month.equals(other.month) && this.year == other.year;
    }
    else
    {
      return false;
    }
  }

  /**
  * returns the date of tommorrow by the current date
  * @return the date of tommorrow by the current date
  */
  public Date tommorrow()
  {
    if (this.day == this.month.nbDays(this.year))
    {
      if (this.month == Month.december)
      {
        return new Date(1, Month.january, this.year+1);
      }
      else
      {
        return new Date(1, this.month.next(), this.year);
      }
    }
    else
    {
      return new Date(this.day + 1, this.month, this.year);
    }
  }

  /**
  * returns a new date with the number of days later
  * @param nbDays the number of days later
  * @return result a new date with the number of days later or otherways return an exception (IllegalArgumentException)
  */
  public Date nDaysLater(int nbDays)
  {
    if (nbDays < 0)
    {
      throw new IllegalArgumentException("Argument should be positive");
    }
    Date result = new Date(this.getDay(), this.getMonth(), this.getYear());
    for(int i = 0; i < nbDays; i++)
    {
      result = result.tommorrow();
    }
    return result;
  }

  /**
  * returns the difference of day between the current date and the date given in parameter
  * @param d a date
  * @return diff the difference of day between the current date and the date given in parameter
  */
  public int diffDays(Date d)
  {
    if (this.compareTo(d) > 0)
    {
      return -(d.diffDays(this));
    }
    int diff = 0;
    Date result = this;
    while(!result.equals(d))
    {
      result = result.tommorrow();
      diff++;
    }
    return diff;
  }

  /**
  * returns -1,0 or 1 if the current date is smaller, equals or greater than the date given in parameter
  * @param d a date
  * @return int
  */
  public int compareTo(Date d)
  {
    if (this.day < d.day && this.month.ordinal() < d.month.ordinal() && this.year < d.year)
    {
      return -1;
    }
    else if (this.equals(d))
    {
      return 0;
    }
    else
    {
      return 1;
    }
  }

  /**
  * returns <code>true</code> <code>if</code> the year given in parameter is bisextile
  * @param year a year
  * @return <code>true</code> <code>if</code> the year given in parameter is bisextile
  */
  public static boolean isBisextile(int year)
  {
    if ((year % 100) != 0 && (year % 4) == 0)
    {
      return true;
    }
    else
    {
      return false;
    }
  }

  /**
  * returns a sentence contains the current dates
  * @return String
  */
  public String toString()
  {
    return "the current date is : "+this.getDay()+ "/" + this.getMonth() + "/" + this.getYear();
  }
}
