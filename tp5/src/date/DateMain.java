package date;
import date.*;


/**
 *  Date  main class to excute some methods of the date class.
 *
 * @author Abdelaziz HAMMAD AHMED
 * @version 1.0
 */

public class DateMain{
    public static void main(String[] args){
        Month m =  Month.december;
        Date d1 = new Date(1, m, 2016);
        System.out.printf("The first date is %s \n",d1.toString());
        Date d2 = d1.nDaysLater(31);
        System.out.printf(" 31 days later the date will be %s \n",d2.toString());
        d2 = d1.nDaysLater(365);
        System.out.printf(" 365 days passed so %s \n",d2.toString());

       
        Date d3 = d1.tommorrow();
        System.out.printf("If yesterday is january 1st 2016 then  %s \n",d3.toString());
        

         int df = d1.diffDays(d2);
        System.out.printf("the diffrence is %d \n", df);


        int comp = d2.compareTo(d1);
        if (comp == 1){
            System.out.println("the second date is after the first one");
        }
        else if(comp == -1){
            System.out.println("the second date is before the first one");
        }
        boolean c2 = d2.equals(d1);
        if(c2){
            System.out.println("the 2 dates are equals");
        }
        else{
            System.out.println("the 2 dates are NOT equals");
        }

    }
}
