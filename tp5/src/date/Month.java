package date;



/**
 * representation of a month
 *
 * @author Abdelaziz HAMMAD AHMED
 * @version 07/10/2020
 */

public enum Month{
january(31), february(28), march(31), april(30), may(30), june(31), july(30), august(31), september(30), october(31), november(30), december(31) ;

  /**
  * attribut of the class month
  */
  private final int nbDays;

  /**
  * the constructor of the class Month, create an instance of month
  * @param nbDays an int which is the number of days
  */
  private Month(int nbDays)
  {
    this.nbDays = nbDays;
  }

  /**
  * return the number of days of the current month by the year given in parameter
  * @param year the year
  * @return nbDays the number of days by the year given in parameter
  */
  public int nbDays(int year)
  {
    if (this != Month.february || ! Date.isBisextile(year))
    {
      return this.nbDays;
    }
    else
    {
      return this.nbDays + 1;
    }
  }

  /**
  * returns the next month
  * @return month
  */
  public Month next()
  {
    return Month.values()[(this.ordinal()+1) % (Month.values().length)];
  }

}
