import org.junit.*;
import static org.junit.Assert.*;

import date.*;


public class DateTest {

    @Test
    public void testDateTommorrow() {
      Date d1 = new Date(11, Month.march, 2020);
      assertEquals(d1.tommorrow(), new Date (12, Month.march, 2020));
      Date d2 = new Date(28, Month.march, 2020);
      assertEquals(d2.tommorrow(), new Date (29, Month.march , 2020));
    }

    @Test (expected = IllegalArgumentException.class)
    public void testNdaysLater() {

      Date d1 = new Date(10, Month.may, 2019);
      assertEquals(d1.nDaysLater(1), new Date(11, Month.may, 2019));
      assertEquals(d1.nDaysLater(-5), new Date(25, Month.may, 2019));
    }

    @Test
    public void testDiffDays()
    {

    }

    @Test
    public void testCompareTo()
    {

      Date d1 = new Date(14, Month.february, 2018);
      Date d2 = new Date(14, Month.february, 2004);
      assertEquals(0, d1.compareTo(d1));
    }

    @Test
    public void testIsBisextile()
    {
      Date d1 = new Date(14, Month.february, 2018);
      assertFalse(d1.isBisextile(d1.getYear()));
      Date d2 = new Date(14, Month.february, 2004);
      assertTrue(d2.isBisextile(d2.getYear()));
    }



    // ---Pour permettre l'exécution des test----------------------
    public static junit.framework.Test suite() {
        return new junit.framework.JUnit4TestAdapter(DateTest.class);
    }

}
