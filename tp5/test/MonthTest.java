import org.junit.*;
import static org.junit.Assert.*;
import date.*;

public class MonthTest {

    @Test
    public void testNbDays() {
      
      Month m1 =  Month.december;
      Month m2 =  Month.january;
      assertEquals(m1.nbDays(31), m2.nbDays(31));
   
    }

    @Test
    public void testNext() {
     
      Month m1 =  Month.december;
      Month m2 =  Month.january;
      assertEquals(m1.next(), m2);

    }



     // ---Pour permettre l'exécution des test----------------------
    public static junit.framework.Test suite() {
        return new junit.framework.JUnit4TestAdapter(MonthTest.class);
    }

}
