import org.junit.*;
import static org.junit.Assert.*;

import factory.Robot;
import factory.util.*;

public class RobotTest {


    @Test
    public void robotCarryNoBoxWhenCreated() {
	    Robot r = new Robot();
	    assertFalse(r.carryBox());
    }


    @Test
    public void robotCanTakeBoxIfNotCarrying() {
      Robot r = new Robot();
      Box b = new Box(10);
	    assertFalse(r.carryBox());
      r.take(b);
      assertSame(b, r.getCarriedBox());
    }

    @Test
    public void robotCannotTakeBoxIfAlreadyCarrying() {
      Robot r = new Robot();
      Box b1 = new Box(10);
      r.take(b1);
      assertSame(b1, r.getCarriedBox());
      Box b2 = new Box(3);
      r.take(b2);
      assertSame(b1, r.getCarriedBox());
    }

    @Test
    public void robotCannotPutBoxOnBeltIfNotCarryingOne() {
      Robot r = new Robot();
      ConveyerBelt belt = new ConveyerBelt(14);
      assertFalse(r.carryBox());
      assertFalse(r.putOn(belt));
    }

    @Test
    public void robotCanPutBoxItHasTakenOnBelt() {
      Robot r = new Robot();
      Box b = new Box(10);
      ConveyerBelt belt = new ConveyerBelt(14);
      r.take(b);
      assertTrue(r.carryBox());
      assertTrue(belt.accept(b));
      assertTrue(!belt.isFull());
      assertTrue(r.putOn(belt));
      assertFalse(r.carryBox());
    }

    @Test
    public void robotCannotPutBoxItHasTakenOnBelt() {
      Robot r = new Robot();
      Box b = new Box(11);
      ConveyerBelt belt = new ConveyerBelt(14);
      r.take(b);
      assertTrue(r.carryBox());
      assertFalse(belt.accept(b));
      assertFalse(r.putOn(belt));
      assertSame(b, r.getCarriedBox());
    }

    @Test
    public void robotCarriesBoxButBeltIsFull() {
      Robot r = new Robot();
      Box b = new Box(11);
      ConveyerBelt belt = new ConveyerBelt(14);
      r.take(b);
      Box b1 = new Box(5);
      Box b2 = new Box(6);
      belt.receive(b1);
      belt.receive(b2);
      assertTrue(r.carryBox());
      assertTrue(belt.isFull());
      assertFalse(r.putOn(belt));
      assertSame(b, r.getCarriedBox());
    }






    // ---Pour permettre l'exécution des test----------------------
    public static junit.framework.Test suite() {
      return new junit.framework.JUnit4TestAdapter(RobotTest.class);
    }

}
